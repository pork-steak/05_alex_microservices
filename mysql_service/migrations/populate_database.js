const mysql = require('mysql2/promise');
const { books, authors } = require('./data.js');

const options = {
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PWD,
  database: 'books_on_a_rock',
};

function connectToMysql() {
  return mysql.createConnection(options);
}

async function insertDataToBooksTable(connection, bookData) {
  console.log('Preparing query');
  let columnValues = [];
  try {
    bookData.forEach((book) => {
      columnValues.push(`('${book.isbn}', '${book.title}', ${book.average_rating},
        '${book.image_url}', ${book.goodreads_id}, '${book.goodreads_url}',
        "${book.description}", ${book.publication_day}, ${book.publication_month},
        ${book.publication_year})`);
    });
    columnValues = columnValues.join(',');
  } catch (err) {
    console.error('Failed preaparing statement, Check data for bad values');
    throw err;
  }

  console.log('Inserting data into books table');
  try {
    await connection.query(`INSERT INTO books (isbn, title, average_rating, image_url,
      goodreads_id, goodreads_url, description, publication_day, publication_month,
      publication_year) VALUES ${columnValues}`);
  } catch (err) {
    console.error('Error with query');
    throw err;
  }
}

async function insertDataToAuthorsTable(connection, authorData) {
  console.log('Preparing query');
  let columnValues = [];
  try {
    authorData.forEach((author) => {
      columnValues.push(`("${author.name}", '${author.image_url}',
      ${author.goodreads_id}, '${author.goodreads_url}',
      '${author.description}')`);
    });
    columnValues = columnValues.join(',');
  } catch (err) {
    console.error('Failed preaparing statement, Check data for bad values');
    throw err;
  }
  // console.log(columnValues);
  console.log('Inserting data into books table');
  try {
    await connection.query(`INSERT INTO authors (name, image_url, goodreads_id,
      goodreads_url, description) VALUES ${columnValues}`);
  } catch (err) {
    console.error('Error with query');
    throw err;
  }
}

async function mapForeignKey(connection) {
  const query1 = 'UPDATE books SET author_1 = 1 WHERE book_id = 1';
  const query2 = 'UPDATE books SET author_1 = 2 WHERE book_id = 2';
  const query3 = 'UPDATE books SET author_1 = 1 WHERE book_id = 3';
  const query4 = 'UPDATE books SET author_1 = 3 WHERE book_id = 4';
  const query5 = 'UPDATE books SET author_1 = 4 WHERE book_id = 5';
  const query6 = 'UPDATE books SET author_1 = 5 WHERE book_id = 6';

  try {
    await connection.query(query1);
  } catch (err) {
    console.error('Error with query');
    throw err;
  }

  try {
    await connection.query(query2);
  } catch (err) {
    console.error('Error with query');
    throw err;
  }

  try {
    await connection.query(query3);
  } catch (err) {
    console.error('Error with query');
    throw err;
  }

  try {
    await connection.query(query4);
  } catch (err) {
    console.error('Error with query');
    throw err;
  }

  try {
    await connection.query(query5);
  } catch (err) {
    console.error('Error with query');
    throw err;
  }

  try {
    await connection.query(query6);
  } catch (err) {
    console.error('Error with query');
    throw err;
  }
}

async function main() {
  let connection;
  try {
    connection = await connectToMysql();
    console.log('Successfully connected to database');
  } catch (err) {
    console.log('Cannot Connect to MySQL database, Check for bad mysql login input');
    console.error(err);
    process.exit();
  }

  try {
    await insertDataToBooksTable(connection, books);
    await insertDataToAuthorsTable(connection, authors);
    await mapForeignKey(connection);
    console.log('Inserted Data successfully');
  } catch (err) {
    console.error(err);
  } finally {
    connection.end();
  }
}

main();
