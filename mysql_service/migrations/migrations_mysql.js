const mysql = require('mysql2/promise');

const options = {
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PWD,
};

function connectToMysql() {
  return mysql.createConnection(options);
}

async function createDatabase(connection) {
  await connection.query('CREATE DATABASE IF NOT EXISTS books_on_a_rock');
  await connection.query('USE books_on_a_rock');
  console.log('Successfully connected to database');
  return connection;
}

async function createBooksTable(connection) {
  console.log('Creating Books Table');
  try {
    await connection.query(`CREATE TABLE books (
      book_id SMALLINT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
      isbn CHAR(10) NOT NULL,
      title VARCHAR(200) NOT NULL,
      average_rating FLOAT(3,2) UNSIGNED,
      image_url VARCHAR(150),
      goodreads_id INT UNSIGNED NOT NULL,
      goodreads_url VARCHAR(150) NOT NULL,
      description TEXT,
      publication_day TINYINT UNSIGNED,
      publication_month TINYINT UNSIGNED,
      publication_year SMALLINT UNSIGNED,
      author_1 SMALLINT UNSIGNED,
      author_2 SMALLINT UNSIGNED,
      author_3 SMALLINT UNSIGNED,
      author_4 SMALLINT UNSIGNED,
      author_5 SMALLINT UNSIGNED,
      author_6 SMALLINT UNSIGNED,
      UNIQUE (isbn, goodreads_id) ) CHARACTER SET utf8 COLLATE utf8_general_ci
      `);
  } catch (err) {
    console.error('Creating books table failed');
    throw err;
  }
}

async function createAuthorsTable(connection) {
  console.log('Creating Authors Table');

  try {
    await connection.query(`CREATE TABLE authors (
      author_id SMALLINT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
      name VARCHAR(200) NOT NULL,
      image_url VARCHAR(150),
      goodreads_id INT UNSIGNED NOT NULL,
      goodreads_url VARCHAR(150) NOT NULL,
      description TEXT,
      UNIQUE (goodreads_id) )
      CHARACTER SET utf8 COLLATE utf8_general_ci`);
  } catch (err) {
    console.error('Creating authors table failed');
    throw err;
  }
}

async function mapForeignKey(connection) {
  console.log('Mapping foreign key from books table to authors table');

  let query = 'ALTER TABLE books ';
  const columnValues = [];

  for (let i = 1; i <= 6; i++) {
    columnValues.push(`ADD CONSTRAINT fk_book_author_${i} FOREIGN KEY (author_${i})
      REFERENCES authors(author_id) ON DELETE CASCADE ON UPDATE CASCADE`);
  }
  query += columnValues.join(',');
  try {
    await connection.query(query);
  } catch (err) {
    console.log('Failed to add foreign key to books table');
    throw err;
  }
}

function closeConnection(connection) {
  return connection.end();
}

async function main() {
  let connection;
  try {
    connection = await connectToMysql();
  } catch (err) {
    console.log('Cannot Connect to MySQL database, Check for bad mysql login input');
    console.error(err);
    process.exit();
  }

  try {
    await createDatabase(connection);
    await createBooksTable(connection);
    console.log('books table created successfully');
    await createAuthorsTable(connection);
    console.log('authors Table created successfully');
    await mapForeignKey(connection);
    console.log('Mapped foreign key from books to authors successfully');
  } catch (err) {
    // errorsLogger.error(err);
    console.error(err.stack);
  } finally {
    closeConnection(connection);
  }
}

main();
