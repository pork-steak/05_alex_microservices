const express = require('express');
const bodyParser = require('body-parser');

const routes = require('./src');

const app = express();
const port = process.env.MYSQL_PORT || 3000;

app.use(bodyParser.json());

app.use((req, res, next) => {
  const allowedOrigins = [
    `http://${process.env.ORIGIN}:${process.env.FRONT_PORT}`,
    `http://${process.env.ORIGIN}:${process.env.BACK_PORT}`,
  ];

  const { origin } = req.headers;

  if (allowedOrigins.includes(origin)) {
    res.setHeader('Access-Control-Allow-Origin', origin);
  }
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE');
  next();
});

app.use((req, res, next) => {
  console.log(req.url);
  next();
});

app.use('/', routes);

app.listen(port, () => console.log(`MySQL service listening on port ${port}!`));
