const mysql = require('mysql2/promise');

const {
  mysqlHost,
  mysqlUser,
  mysqlPwd,
} = require('../../config.js');

let database = 'books_on_a_rock';

if (process.env.NODE_ENV === 'dev') {
  database = 'dummy_rock';
}

const pool = mysql.createPool({
  host: mysqlHost,
  user: mysqlUser,
  password: mysqlPwd,
  database,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
});

module.exports.getAuthors = () => pool.query('SELECT * FROM authors');

module.exports.getAuthorById = (id) => {
  const query = `SELECT a.*, b.title, b.book_id FROM authors a
    RIGHT JOIN books b ON b.author_1 = a.author_id
    WHERE a.author_id = ?`;
  return pool.execute(query, [id]);
};

module.exports.addAuthor = (authorDetail) => {
  const query = `INSERT INTO authors (name, image_url, goodreads_id, goodreads_url, description)
    VALUES (?, ?, ?, ?, ?)`;
  return pool.execute(query, authorDetail);
};

module.exports.updateAuthor = (fields, values, id) => {
  const query = `UPDATE authors SET ${fields.join(',')} WHERE author_id = ?`;
  return pool.execute(query, [...values, id]);
};

module.exports.deleteAuthor = (id) => {
  const query = 'DELETE FROM authors WHERE author_id = ?';
  return pool.execute(query, [id]);
};
