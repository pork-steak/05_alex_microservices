const express = require('express');

const {
  getBooks,
  getBookById,
  addBook,
  updateBook,
  deleteBook,
} = require('./booksModel.js');

const router = express.Router();
module.exports = router;

// books [{}, {}, ...]
router.get('/', async (req, res, next) => {
  try {
    const [books] = await getBooks();
    if (books.length > 0) {
      res.json(books);
    }
  } catch (err) {
    next(err);
  }
});

// get book by ID / book [{}]
router.get('/:bookId', async (req, res, next) => {
  try {
    const [book] = await getBookById(+req.params.bookId);
    if (book.length === 0) {
      res.status(404);
      res.json({ error: 'no resource found using the specified book id' });
      next(new Error('no books found'));
    } else {
      res.json(book[0]);
    }
  } catch (err) {
    next(err);
  }
});

// add a new book
router.post('/', async (req, res, next) => {
  const bookDetails = Object.values(req.body);
  try {
    const [field] = await addBook(bookDetails);
    if (field.affectedRows === 1) {
      res.json({ message: 'Successfully Added' });
    }
  } catch (err) {
    res.status(500);
    res.json({ error: err.message });
    next(err);
  }
});

// update a book by ID
router.put('/:bookId', async (req, res, next) => {
  // console.log('put');
  const fields = [];
  const values = [];
  if (req.body.isbn) {
    fields.push('isbn=?');
    values.push(req.body.isbn);
  }

  if (req.body.title) {
    fields.push('title=?');
    values.push(req.body.title);
  }

  if (req.body.imageUrl) {
    fields.push('image_url=?');
    values.push(req.body.imageUrl);
  }

  if (req.body.goodreadsId) {
    fields.push('goodreads_id=?');
    values.push(+req.body.goodreadsId);
  }

  if (req.body.goodreadsUrl) {
    fields.push('goodreads_url=?');
    values.push(req.body.goodreadsUrl);
  }

  if (req.body.description) {
    fields.push('description=?');
    values.push(req.body.description);
  }

  if (req.body.publicationDay) {
    fields.push('publication_day=?');
    values.push(req.body.publicationDay);
  }

  if (req.body.publicationMonth) {
    fields.push('publication_month=?');
    values.push(req.body.publicationMonth);
  }

  if (req.body.publicationYear) {
    fields.push('publication_year=?');
    values.push(req.body.publicationYear);
  }

  if (req.body.author1) {
    fields.push('author_1=?');
    values.push(req.body.author1);
  }

  try {
    const [field] = await updateBook(fields, values, +req.params.bookId);
    if (field.affectedRows) {
      res.json({ message: 'Successfully updated the changes' });
    }
  } catch (err) {
    res.json({ error: err.message });
    next(err);
  }
});

// delete book by ID
router.delete('/:bookId', async (req, res, next) => {
  try {
    const id = req.params.bookId;
    const [field] = await deleteBook(id);
    if (field.affectedRows === 0) {
      res.json({ error: "The book ID you've specified does not exists" });
    } else if (field.affectedRows === 1) {
      res.json({ message: `Successfully deleted book with id ${id}` });
    }
  } catch (err) {
    res.json({ error: err.message });
    next(err);
  }
});
