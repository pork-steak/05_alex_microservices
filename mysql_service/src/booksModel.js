const mysql = require('mysql2/promise');

const {
  mysqlHost,
  mysqlUser,
  mysqlPwd,
} = require('../../config.js');

let database = 'books_on_a_rock';

if (process.env.NODE_ENV === 'dev') {
  database = 'dummy_rock';
}

const pool = mysql.createPool({
  host: mysqlHost,
  user: mysqlUser,
  password: mysqlPwd,
  database,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
});

module.exports.getBooks = () => {
  const query = `SELECT b.*, a.author_id, a.name FROM books b
    LEFT JOIN authors a ON b.author_1 = a.author_id`;
  return pool.execute(query);
};

module.exports.getBookById = (id) => {
  const query = 'SELECT * FROM `books` WHERE `book_id` = ?';
  return pool.execute(query, [id]);
};

module.exports.addBook = (bookDetail) => {
  const query = `INSERT INTO books
    (isbn, title, average_rating, image_url, goodreads_id,
    goodreads_url, description, publication_day, publication_month, publication_year, author_1)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
  return pool.execute(query, bookDetail);
};

module.exports.updateBook = (fields, values, id) => {
  const query = `UPDATE books SET ${fields.join(',')} WHERE book_id = ?`;
  return pool.execute(query, [...values, id]);
};

module.exports.deleteBook = (id) => {
  const query = 'DELETE FROM books WHERE book_id = ?';
  return pool.execute(query, [id]);
};
