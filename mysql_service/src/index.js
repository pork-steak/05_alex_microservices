const express = require('express');

const books = require('./booksRouter.js');
const authors = require('./authorsRouter.js');

const router = express.Router();

// index router
module.exports = router;

router.use((req, res, next) => {
  console.log(req.url);
  next();
});

router.use('/books', books);
router.use('/authors', authors);

router.use((req, res, next) => {
  res.status(404);
  res.json({ error: 'resource not found' });
  // res.sendFile('404page.html', { root: './public' });
  next();
});

router.use((err, req, res, next) => {
  if (err) {
    console.error(err.message);
    console.error(err.stack);
  }
  next();
});
