const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const session = require('express-session');
const favicon = require('serve-favicon');
const MongoStore = require('connect-mongo')(session);

const routes = require('./src');
const {
  frontendPort,
  mongoHost,
  mongoUser,
  mongoPwd,
} = require('../config.js');

const app = express();
const port = frontendPort || 3002;

const mongoUri = `mongodb://${mongoUser}:${mongoPwd}@${mongoHost}/users`;
const connection = mongoose.createConnection(mongoUri, { useNewUrlParser: true });

connection.on('error', err => console.log(err.message));

app.use(favicon(`${__dirname}/public/favicon.ico`));
app.use(session({
  resave: false,
  saveUninitialized: false,
  secret: 'My Secret',
  cookie: {
    maxAge: 60 * 60 * 12 * 1000,
  },
  store: new MongoStore({ mongooseConnection: connection }),
}));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static('public'));
app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(routes);

app.listen(port, () => console.log(`Frontend service app listening on port ${port}!`));
