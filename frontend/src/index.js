const express = require('express');

const index = require('./indexRouter.js');
const user = require('./userRouter.js');
const books = require('./booksRouter.js');
const authors = require('./authorsRouter.js');
const {
  isLoggedIn,
  noCache,
  trackLogin,
  logRequestedRoutes,
  page404,
  errorHandler,
} = require('./middleware/commonMiddleware.js');

const router = express.Router({ strict: true });

router.use(logRequestedRoutes);
router.use(trackLogin);

router.use('/', index);
router.use(['/user/login', '/user'], user);
router.use('/books', isLoggedIn, noCache, books);
router.use('/authors', isLoggedIn, noCache, authors);
router.use(page404);
router.use(errorHandler);

module.exports = router;
