const express = require('express');
const rp = require('request-promise');
const { mysqlApi } = require('../../config.js');

const router = express.Router();
module.exports = router;

router.get('/', async (req, res, next) => {
  try {
    let authors = await rp(`${mysqlApi}/authors`);
    authors = JSON.parse(authors);
    res.render('authors', { authors });
  } catch (err) {
    res.status(err.statusCode);
    next(err);
  }
});

router.get('/:authorId', async (req, res, next) => {
  try {
    let rows = await rp(`${mysqlApi}/authors/${req.params.authorId}`);
    rows = JSON.parse(rows);
    const result = {
      author: rows[0],
      books: [],
    };
    rows.forEach((entry) => {
      result.books.push({ title: entry.title, id: entry.book_id });
    });
    res.render('author', { author: result.author, books: result.books });
  } catch (err) {
    next(err);
  }
});
