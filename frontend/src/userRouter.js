const express = require('express');
const rp = require('request-promise');

const { validateInput } = require('./middleware/commonMiddleware.js');
const { mongoApi } = require('../../config.js');

const router = express.Router();
module.exports = router;

router.get('/', (req, res) => {
  res.render('login', { message: '' });
});

router.post('/', async (req, res, next) => {
  if (!req.body.username || !req.body.password) {
    res.render('login', { message: 'Username and Password required' });
  } else {
    const options = {
      method: 'POST',
      body: {
        username: req.body.username,
        password: req.body.password,
      },
      json: true,
    };
    try {
      const result = await rp(mongoApi, options);
      if (!result) {
        res.render('login', { message: 'Invalid username/password' });
      } else {
        req.session.loggedIn = true;
        req.session.user = result.user;
        res.redirect('/');
      }
    } catch (err) {
      res.render('login', { message: 'Invalid username/password' });
      next(err);
    }
  }
});

router.get('/signup', (req, res) => {
  res.render('signup', { success: false });
});

router.post('/signup', validateInput, async (req, res, next) => {
  if (req.invalid) {
    const message = {
      title: 'Form Error',
      body: req.invalid,
    };
    res.render('signup', { success: true, message });
  } else {
    const options = {
      method: 'POST',
      body: {
        username: req.body.username,
        password: req.body.password,
      },
      json: true,
    };
    try {
      const result = await rp(`${mongoApi}/signup`, options);
      if (result.error) {
        const message = {
          title: 'Error',
          body: result.error,
        };
        res.render('signup', { success: true, message });
      } else {
        res.redirect('/user');
      }
    } catch (err) {
      const message = {
        title: err.statusCode,
        body: err.error,
      };
      res.render('signup', { success: true, message });
      next(err);
    }
  }
});

router.post('/logout', (req, res) => {
  if (req.session) {
    req.session.destroy(() => {
      res.locals.loggedIn = false;
      res.redirect('back');
    });
  }
});
