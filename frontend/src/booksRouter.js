const express = require('express');
const rp = require('request-promise');
const { mysqlApi } = require('../../config.js');

const router = express.Router();

module.exports = router;

router.get('/', async (req, res, next) => {
  try {
    let books = await rp(`${mysqlApi}/books`);
    books = JSON.parse(books);
    res.render('books', { books });
  } catch (err) {
    res.status(err.statusCode);
    next(err);
  }
});

router.get('/:bookId', async (req, res, next) => {
  try {
    let book = await rp(`${mysqlApi}/books/${req.params.bookId}`);
    book = JSON.parse(book);
    res.render('book', { book });
  } catch (err) {
    res.status(err.statusCode);
    next(err);
  }
});

// module.exports = router.get('/isbn/:isbn', async (req, res, next) => {
//   try {
//     const book = await getGrReview(req.params.isbn);
//     if (!book.reviews) {
//       book.reviews = 'No reviews found';
//     }
//     res.render('book', { book });
//   } catch (err) {
//     next(err);
//   }
// });
