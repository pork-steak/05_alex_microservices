// const {
//   errorsLogger,
//   combinedLogger,
// } = require('../../../config/winston_config');

module.exports.logRequestedRoutes = (req, res, next) => {
  // combinedLogger.info(`requested route: ${req.url}`);
  console.info(`requested route: ${req.url}`);
  next();
};

module.exports.isLoggedIn = (req, res, next) => {
  if (!req.session.loggedIn) {
    res.redirect('/user');
  } else {
    next();
  }
};

module.exports.trackLogin = (req, res, next) => {
  res.locals.loggedIn = !!req.session.loggedIn;
  next();
};

module.exports.validateInput = (req, res, next) => {
  if (!req.body.password || !req.body.username) {
    req.invalid = 'Username and Password cannot be blank';
    next();
  } else {
    const input = req.body.username.match(/^[\w]{3,}$/);
    if (!input) {
      req.invalid = 'Invalid Username and/or password';
    }
    if (req.body.password.length < 6) {
      req.invalid = 'Password should be atleast 6 characters';
    }
    next();
  }
};

module.exports.noCache = (req, res, next) => {
  res.set(
    'Cache-Control',
    'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0',
  );
  next();
};

module.exports.page404 = (req, res, next) => {
  res.status(404);
  res.render('404page');
  next();
};

module.exports.errorHandler = (err, req, res, next) => {
  if (err) {
    if (res.statusCode === 404) {
      res.render('404page');
    }
    // errorsLogger.error();
    // combinedLogger.info(err.message);
    // errorsLogger.error(err.stack);
    // console.log(err);
    console.error(err.message);
    console.error(err.stack);
  }
  next();
};
