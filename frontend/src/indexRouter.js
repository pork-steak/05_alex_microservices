const express = require('express');
const rp = require('request-promise');
const { mysqlApi } = require('../../config.js');

const router = express.Router();
module.exports = router;

router.get('/', async (req, res, next) => {
  try {
    let books = await rp(`${mysqlApi}/books`);
    books = JSON.parse(books);
    if (books.length > 0) {
      res.render('index', { books });
    }
  } catch (err) {
    next(err);
  }
});

router.get('/about', (req, res) => {
  res.render('about');
});

router.get('/contact', (req, res) => {
  res.render('contact');
});
