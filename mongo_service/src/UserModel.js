const mongoose = require('mongoose');
const bluebird = require('bluebird');
const bcrypt = require('bcrypt');

const { mongoHost, mongoUser, mongoPwd } = require('../../config.js');

mongoose.Promise = bluebird;
const mongoUri = `mongodb://${mongoUser}:${mongoPwd}@${mongoHost}/users`;

mongoose.connect(mongoUri, { useNewUrlParser: true });

mongoose.connection.on('error', err => console.log(err.message));
const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, 'Sign Up without username, really?'],
  },
  password: {
    type: String,
    required: true,
    minlength: [6, 'Password need to be more secure'],
  },
}, { autoIndex: false });

/* eslint prefer-arrow-callback: 0, func-names: 0 */
userSchema.pre('save', async function (next) {
  try {
    const hash = await bcrypt.hash(this.password, 10);
    this.password = hash;
  } catch (err) {
    next(err);
  }
  next();
});

module.exports.User = mongoose.model('User', userSchema);
