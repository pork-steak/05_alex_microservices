const express = require('express');

const { createUser, authenticate } = require('./userController');

// index router
const router = express.Router();
module.exports = router;

router.get('/', (req, res, next) => {
  res.status(404);
  res.json({ error: 'No GET resource found at this api' });
  next(new Error('No GET resource found at this api'));
});

// respond false on failure or user _id on success
router.post('/', authenticate, (req, res) => {
  if (req.invalid) {
    res.json(false);
  } else {
    res.json({ user: req.user });
  }
});

// respond true on success or json with error messasge
router.post('/signup', async (req, res, next) => {
  try {
    await createUser({ username: req.body.username, password: req.body.password });
    res.json(true);
  } catch (err) {
    res.status(200);
    if (err.code === 11000) {
      const body = 'Username already taken';
      res.json({ error: body });
    }
    next(err);
  }
});

// router.delete()

router.use((req, res, next) => {
  console.log('here');
  res.status(404);
  res.json({ error: 'resource not found' });
  next();
});

router.use((err, req, res, next) => {
  if (err) {
    console.log('in error');
    if ((res.statusCode !== 404) && (res.statusCode !== 200)) {
      console.log('sending again');
      res.status(500);
      res.json('Internatl Server Error! Please contact admin');
    }
    console.error(err.message);
    console.error(err.stack);
  }
  next();
});
