const bcrypt = require('bcrypt');

const { User } = require('./UserModel.js');

/* eslint no-underscore-dangle: 0 */
module.exports.authenticate = async (req, res, next) => {
  let user;
  try {
    user = await User.findOne({ username: req.body.username });
    if (!user) {
      req.invalid = true;
      next();
    } else {
      try {
        const result = await bcrypt.compare(req.body.password, user.password);
        if (result) {
          req.user = user._id;
          next();
        } else {
          req.invalid = true;
          next();
        }
      } catch (err) {
        next(err);
      }
    }
  } catch (err) {
    next(err);
  }
};

module.exports.createUser = async (userObject) => {
  try {
    await User.create(userObject);
  } catch (err) {
    throw err;
  }
};

module.exports.deleteUser = async (username) => {
  let result = false;
  try {
    await User.deleteOne({ username });
    result = true;
  } catch (err) {
    console.log(err);
  }
  return result;
};
