const express = require('express');
const bodyParser = require('body-parser');

const {
  mongoPort,
  address,
  frontendPort,
  backendPort,
} = require('../config.js');

const routes = require('./src');

const app = express();
const port = mongoPort || 3001;

app.use(bodyParser.json());

app.use((req, res, next) => {
  const allowedOrigins = [
    `http://${address}:${frontendPort}`,
    `http://${address}:${backendPort}`,
  ];
  const { origin } = req.headers;
  if (allowedOrigins.includes(origin)) {
    res.setHeader('Access-Control-Allow-Origin', origin);
  }
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE');
  next();
});

app.use((req, res, next) => {
  console.log(req.url);
  next();
});

app.use('/', routes);

app.listen(port, () => console.log(`MongoDB service listening on port ${port}!`));
