/*
 * export in your .bashrc or .bash_profile or other bash settings like so
 * MYSQLDB_USER=username (replace 'username' with your mysql user)
 * accessed as process.env.MYSQLDB_USER (note the capitalization)
 * if you don't have env variables, replace process.env.VARIABLE with your own credentials
 */

// mysql secret config
module.exports.mysqlHost = process.env.MYSQLDB_HOST;
module.exports.mysqlUser = process.env.MYSQLDB_USER;
module.exports.mysqlPwd = process.env.MYSQLDB_PWD;

// mongoDB secret config
module.exports.mongoHost = process.env.MONGODB_HOST;
module.exports.mongoUser = process.env.MONGODB_USER;
module.exports.mongoPwd = process.env.MONGODB_PWD;

// server DNS and ports
module.exports.address = process.env.ORIGIN;
module.exports.mysqlPort = process.env.MYSQL_PORT;
module.exports.mongoPort = process.env.MONGODB_PORT;
module.exports.frontendPort = process.env.FRONT_PORT;
module.exports.backendPort = process.env.BACK_PORT;

// api
module.exports.mysqlApi = `http://${process.env.ORIGIN}:${process.env.MYSQL_PORT}`;
module.exports.mongoApi = `http://${process.env.ORIGIN}:${process.env.MONGODB_PORT}`;
