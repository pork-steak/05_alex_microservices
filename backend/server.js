const express = require('express');

const { backendPort } = require('../config.js');
const port = backendPort || 3003;
const app = express();

app.use(express.static(`${__dirname}/public/build`));
app.use(express.static(`${__dirname}/public`));

app.get('/', (req, res) => {
  res.sendFile(`${__dirname}/public/build/index.html`);
});

app.get('*', (req, res) => {
  res.sendFile(`${__dirname}/public/build/index.html`);
});

app.listen(port, () => console.log(`React service listening on port ${port}`));
